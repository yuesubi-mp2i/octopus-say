#!/bin/bash

BASHRC="$HOME/.bashrc"

POULPY_SAY_REMOTE="https://gitlab.com/yuesubi-mp2i/octopus-say/-/raw/main/poulpy_say.py"
POULPY_SAY_DESTINATION="$HOME/.config/poulpy_say.py"

POULPY_BASHRC_MSG="# Poulpy est passé par là"
POULPY_SAY="python3 $POULPY_SAY_DESTINATION | lolcat"


############################################
# Install lolcat

sudo apt install -y lolcat


######################
# Download poulpy_say

curl -s -o "$POULPY_SAY_DESTINATION" "$POULPY_SAY_REMOTE"


###################
# Modify the bashrc

poulpy_occurences=$(grep -c "$POULPY_BASHRC_MSG" "$BASHRC")

if [ $poulpy_occurences == "0" ]; then
    echo "" >> $BASHRC
    echo $POULPY_BASHRC_MSG >> $BASHRC
    echo $POULPY_SAY >> $BASHRC
fi
