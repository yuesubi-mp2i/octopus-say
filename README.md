# Poulpy say
Un magnifique Poulpy qui récite une citation de français-philosophie.

<img src="https://gitlab.com/yuesubi-mp2i/octopus-say/-/raw/main/screenshot.png">

## Instructions d'installation

### Rapide
Si vous utilisez `apt` (Debian, Ubuntu, etc...) et `bash` vous pouvez utiliser :
```bash
curl https://gitlab.com/yuesubi-mp2i/octopus-say/-/raw/main/install.sh | bash
```

#### Désinstaller
Si vous avez le mauvais goût de vouloir supprimer poulpy-say,
retirez ces deux lignes de `~/.bashrc` :
```bash
# Poulpy est passé par là
python3 /home/nix/.config/poulpy_say.py | lolcat
```

### Longue
Installez `python` et `lolcat`. Par example :
```bash
# Avec apt (debian, ubuntu, etc...) :
sudo apt install lolcat
# Avec pacman (arch, manjaro, etc...) :
sudo pacman -S lolcat
```

Téléchargez le repo et affichez le poulpe :
```bash
git clone https://gitlab.com/yuesubi-mp2i/octopus-say.git
cd octopus-say/
python3 poulpy_say.py | lolcat
```

Pour une citation à l'ouverture du terminal :
Ajoutez cette dernière ligne à la fin de votre `~/.bashrc`.

(Pour les autres shells, vous savez vous débrouiller.)

## Contributions
Si vous trouvez des erreurs contactez moi sur discord.

## Crédit
`octopus.cow` : https://github.com/paulkaefer/cowsay-files/