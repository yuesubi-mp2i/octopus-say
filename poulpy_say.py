import random


TEMPS = "Le Temps de l'innocence, Wharton"
TRAITE = "Traité théologico-politique, Spinoza"
SUPPL = "Les Suppliantes, Eschyle"
THEBES = "Les Sept contre Thèbes, Eschyle"


TEXT_WIDTH = 40

WONDERFUL_OCTOPUS = """    O    ,------.
     o  ⎛        ⎞
      ° |(O) (O) |
        '◟_   __ノ_
       |`-╯ . .__-'
        `--'`.__ゝ  yue"""


FORTUNES = {
    (24, 10, 3): {
        SUPPL: [
            (56,
                "Zeus ! C’est Io, hélas ! Que poursuit en nous un courroux divin : je reconnais une jalousie d’épouse, qui triomphe du Ciel tout entier. Elle est âpre, la bourrasque d’où va sortir l’ouragan !"
            ),
            (52,
                "Mais, d’abord, ma voix au-delà des mers ira appeler mon soutien, le jeune taureau né de Zeus et de la génisse qu’on vit paître ici des fleurs ; sous le souffle de Zeus, sous le toucher qui, naturellement, lui donna son nom, Épaphos."
                + "\nC’est en invoquant ce nom, en rappelant aujourd’hui, aux lieux mêmes où jadis paissait mon antique aïeule, ses malheurs d’autrefois, que je fournirai à ce pays des indices de ma naissance"
            ),
            (60,
                "Je parlerai bref et net. Nous nous honorons d’être de race argienne et de descendre d’une génisse féconde. Tout cela est vrai, et, si je puis parler, je saurai l’établir."
            ),
            (61,
                "ton récit concorde avec le mien !"
            ),
            (62,
                "Vous semblez en effet avoir d’antiques liens avec ce pays."
            ),
            (54,
                "Qu’il jette donc les yeux sur la démesure humaine, incarnée à nouveau dans la race qui, pour obtenir mon hymen, s’épanouit en funestes et folles pensées !"
            ),
            (68,
                "la démesure de la troupe mal fera horreur à notre peuple"                
            ),
            (69,
                "des monstres plus cruels que le plus cruel serpent ?"
            ),
            (68,
                "Je suis contraint de respecter le courroux de Zeus Suppliant : il n’est pas pour les mortels de plus haut objet d’effroi"
            ),
            (67,
                "Une masse de maux vient sur moi comme un fleuve, et me voici au large d’une mer de douleurs, mer sans fond"
            ),
            (54,
                "Zeus précipite les mortels du haut de leurs espoirs superbes dans le néant"
            ),
            (53,
                "Allons, divins auteurs de ma naissance, vous voyez où est le Droit : exaucez nous ! Ou, si le Destin ne veut pas que le Droit ait satisfaction pleine, du moins, dans votre haine toujours prête à frapper la démesure, montrez votre justice en face de cet hymen."
            ),
            (55,
                "quand la mort est là, qui menace. Hélas ! vents incertains ! Où nous emportera ce flot ?"
            ),
            (69,
                "LE ROI : Jamais roi n’a connu la peur"
            ),
            (65,
                "LE ROI : Décider ici n’est point facile : ne t’en remets pas à moi pour décider. Je te l’ai dit déjà : quel que soit mon pouvoir, je ne saurais rien faire sans le peuple."
            ),
            (65,
                "LE ROI : (...) ce que l’on dirait si le roi prenait une décision sans le peuple : « Pour honorer des étrangers, tu as perdu ta cité ! »"
            ),
            (64,
                "LE ROI : Je ne sais que faire ; l’angoisse prend mon cœur : dois-je agir ou ne pas agir ? Dois-je tenter le Destin ?"
            )
        ],

        THEBES: [
            (150,
                "ÉTÉOCLE : Je ne te dénie point le droit d’honorer les dieux ; mais, si tu ne veux pas semer la lâcheté au cœur des citoyens, reste en repos, ne laisse pas déborder ta terreur."
            ),
            (143,
                "ÉTÉOCLE : (...) en cas de succès, aux dieux tout le mérite ! Si au contraire – ce qu’au Ciel ne plaise ! - un malheur arrive, « Étéocle ! » - un seul nom dans des milliers de bouches"
            ),
            (162,
                "décide seul du coup de barre à donner à la cité"
            )
        ]
    }
}


def cut_in_lines(text: str) -> str:
    lines = [[]]

    for word in text.split(' '):
        len_curr_line = sum([len(w) + 1 for w in lines[-1]])

        if len(word) + len_curr_line > TEXT_WIDTH:
            lines.append([])
        lines[-1].append(word)

    return [' '.join(l) for l in lines]


def make_bubble(text: str) -> str:
    lines = [
        line
        for default_line in text.split('\n')
        for line in cut_in_lines(default_line)
    ]

    max_len = max([len(l) for l in lines])
    lines = [l + ' ' * (max_len - len(l)) for l in lines]
    
    if len(lines) == 1:
        lines = ["( " + lines[0] + " )"]
    else:
        # lines = [()]
        lines = (
            [" _" + '_' * max_len + "_ ",
                "/ " + lines[0] + " \\"]
            + ["| " + l + " |" for l in lines[1:-1]]
            + ["\\ " + lines[-1] + " /",
                " -" + '-' * max_len + "- "]
        )

    return '\n'.join(lines)


def format_date(date: (int, int, int)) -> str:
    yy, mm, dd = date
    return f"{yy}.{mm}.{dd}"


def main():
    fortunes = [
        f"{citation}\n\n-- {book}, p{page} ({format_date(date)})"
        for date, books in FORTUNES.items()
        for book, citations in books.items()
        for page, citation in citations
    ]

    print(make_bubble(random.choice(fortunes)))
    print(WONDERFUL_OCTOPUS)


if __name__ == "__main__":
    main()
